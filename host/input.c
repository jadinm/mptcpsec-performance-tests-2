#include <getopt.h>
#include <errno.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>

#include "../include/main.h"


static inline void set_output_directory(const char *output_path) {

    snprintf(output_directory, MAX_PATH_SIZE, "%s", output_path);
    if (output_path[strlen(output_path)-1] == '/') {
        output_directory[strlen(output_path)-1] = '\0';
    }
}

static inline void set_executable_directory(const char *executable_path) {

    size_t i;

    snprintf(executable_directory, MAX_PATH_SIZE, "%s", executable_path);

    for (i = strlen(executable_path); executable_directory[i] != '/' && i >= 0; i--);

    if (i < 0) { /* Not found */
        executable_directory[0] = '.';
        executable_directory[1] = '\0';
    } else {
        executable_directory[i] = '\0';
    }
}

static inline int parse_interval(char *str, long *start, long *step, long *end) {

    int i;
    int semi_colon_saw = 0;
    char *ptr = NULL;

    for(i = 0; str[i] != '\0'; i++) {
        if (str[i] == ':') {
            str[i] = '\0';
            semi_colon_saw++;
        }
    }

    if (semi_colon_saw != 2)
        return 1;

    *start = strtol(str, &ptr, 10);
    if (*ptr != '\0' || *start <= 0) {
        fprintf(stderr, "Valid start of the interval not found\n");
        return 1;
    }
    str += (strlen(str) + 1);
    *step = strtol(str, &ptr, 10);
    if (*ptr != '\0' || *step <= 0) {
        fprintf(stderr, "Valid step of the interval not found\n");
        return 1;
    }
    str += (strlen(str) + 1);
    *end = strtol(str, &ptr, 10);
    if (*ptr != '\0' || *end <= 0 || *end < *start) {
        fprintf(stderr, "Valid end of the interval not found - end\n");
        return 1;
    }

    return 0;
}

static inline int parse_key_value(char *str, char **key, char **value) {

    int i;

    for(i = 0; str[i] != '\0' && str[i] != '='; i++);

    if (str[i] == '\0')
        return 1;

    *key = str;
    str[i] = '\0';
    *value = &str[i+1];
    return 0;
}

int argument_parsing(int argc, char *argv [], int *no_capture, long *bytes_under_attack, long *start_bulk,
                     long *step_bulk, long *end_bulk, long *start_pingpong, long *step_pingpong, long *end_pingpong,
                     long *iterations, char **serverIP, char **serverPort, int *protocol) {

    int opt = 0;
    int err = 0;
    char *ptr = NULL;

    char *loss_str = NULL;
    char *intf = NULL;
    float loss = 0;
    int use_current_directory = 1;

    /* Arguments default values */

    *no_capture = 1;
    *bytes_under_attack = 0;
    *start_bulk = -1;
    *step_bulk = -1;
    *end_bulk = -1;
    *start_pingpong = -1;
    *step_pingpong = -1;
    *end_pingpong = -1;
    *iterations = 1;

    list_losses_server = init_list();
    list_losses_client = init_list();


    /* Set execution path */

    set_executable_directory(argv[0]);

    /* Options parsing */

    while ((opt = getopt(argc, argv, ":hcb:p:r:l:L:d:D:o:a:")) != -1) {
        switch (opt) {
            case 'h':
                return -1;
            case 'c':
                *no_capture = 0;
                break;
            case 'a':
                *bytes_under_attack = strtol(optarg, &ptr, 10);
                if (*ptr != '\0' || *bytes_under_attack <= 0) {
                    errno = EINVAL;
                    fprintf(stderr, "Option -%c have and invalid argument\n", opt);
                    return 1;
                }
                break;
            case 'b':
                err = parse_interval(optarg, start_bulk, step_bulk, end_bulk);
                if (err) {
                    errno = EINVAL;
                    fprintf(stderr, "Option -%c have and invalid argument\n", opt);
                    return 1;
                }
                break;
            case 'p':
                err = parse_interval(optarg, start_pingpong, step_pingpong, end_pingpong);
                if (err) {
                    errno = EINVAL;
                    fprintf(stderr, "Option -%c have and invalid argument\n", opt);
                    return 1;
                }
                break;
            case 'r':
                *iterations = strtol(optarg, &ptr, 10);
                if (*ptr != '\0' || *iterations <= 0) {
                    errno = EINVAL;
                    fprintf(stderr, "Option -%c have and invalid argument\n", opt);
                    return 1;
                }
                break;
            case 'l':
                loss_str = NULL;
                intf = NULL;
                err = parse_key_value(optarg, &intf, &loss_str);
                if (err) {
                    errno = EINVAL;
                    fprintf(stderr, "Option -%c have and invalid argument\n", opt);
                    return 1;
                }
                loss = strtof(loss_str, &ptr);
                if (*ptr != '\0' || loss < 0.0 || loss >= 100.0) {
                    errno = EINVAL;
                    fprintf(stderr, "Loss values must be inside [0.0, 100.0[\n");
                    return 1;
                }
                init_loss_element(list_losses_client, intf, loss);
                break;
            case 'd':
                //TODO
                break;
            case 'L':
                loss_str = NULL;
                intf = NULL;
                err = parse_key_value(optarg, &intf, &loss_str);
                if (err) {
                    errno = EINVAL;
                    fprintf(stderr, "Option -%c have and invalid argument\n", opt);
                    return 1;
                }
                loss = strtof(loss_str, &ptr);
                if (*ptr != '\0' || loss < 0.0 || loss >= 100.0) {
                    errno = EINVAL;
                    fprintf(stderr, "Loss values must be inside [0.0, 100.0[\n");
                    return 1;
                }
                init_loss_element(list_losses_server, intf, loss);
                break;
            case 'D':
                //TODO
                break;
            case 'o':
                set_output_directory(optarg);
                use_current_directory = 0;
                break;
            case ':':
                errno = EINVAL;
                fprintf(stderr, "Option -%c requires an operand\n", optopt);
                return 1;
            default: /* '?' */
                errno = EINVAL;
                fprintf(stderr, "Unrecognized option: -%c\n", optopt);
                return 1;
        }
    }

    if (use_current_directory)
        set_output_directory(".");

    /* Other argument parsing */

    if (argc - optind != 3) {
        errno = EINVAL;
        perror("Invalid number of arguments ");
        return 1;
    }

    *serverIP = argv[optind++];
    *serverPort = argv[optind++];
    strtol(*serverPort, &ptr, 10);
    if (*ptr != '\0') {
        errno = EINVAL;
        perror("Not a valid port number :\n");
        return 1;
    }

    if (!strcmp(argv[optind], "mptcpsec")) {
        *protocol = MPTCPSEC;
    } else if (!strcmp(argv[optind], "tls")) {
        *protocol = TLS;
        SSL_library_init();
        SSL_load_error_strings();
        ERR_load_BIO_strings();
        OpenSSL_add_all_algorithms();
    } else if (!strcmp(argv[optind], "mptcp")) {
        *protocol = MPTCP;
    } else {
        errno = EINVAL;
        perror("Not a valid protocol :\n");
        return 1;
    }

    return 0;
}
