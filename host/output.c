#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "../include/main.h"
#include "../include/loss.h"

#define MAX_LINE_SIZE 300


static void directory_name(const char *file_path, char *folder_path) {

    size_t i;

    snprintf(folder_path, MAX_PATH_SIZE, "%s", file_path);

    for (i = strlen(folder_path); folder_path[i] != '/' && i >= 0; i--);

    if (i < 0) { /* Not found */
        folder_path[0] = '.';
        folder_path[1] = '\0';
    } else {
        folder_path[i] = '\0';
    }
}

static void recursive_silent_mkdir(const char *file_path) {

    char sub_folder[MAX_PATH_SIZE];
    char *p = NULL;
    size_t len;
    struct stat stats;

    directory_name(file_path, sub_folder);

    len = strlen(sub_folder);

    if(sub_folder[len - 1] == '/') {
        sub_folder[len - 1] = '\0';
    }

    for(p = sub_folder + 1; *p; p++) {
        if (*p == '/') {
            *p = '\0';
            if (stat(sub_folder, &stats) == -1) {
                mkdir(sub_folder, 0777);
            }
            *p = '/';
        }
    }

    if (stat(sub_folder, &stats) == -1) {
        mkdir(sub_folder, 0777);
    }
}

int store_time_measures(const char *file_path, struct timeval *results, size_t results_length) {

    char *line;
    size_t nbytes = 1;
    int i;

    FILE *file = fopen(file_path, "w");

    if (!file) {
        perror("Cannot create/open file");
        return 1;
    }

    line = malloc(MAX_LINE_SIZE);

    for (i = 0; i < results_length && nbytes > 0; i++) {
        snprintf(line, MAX_LINE_SIZE, "%ld.%ld\n", (long int) (results[i].tv_sec), (long int) results[i].tv_usec);
        nbytes = fwrite((void *) line, strlen(line), (size_t) 1, file);
        if (nbytes < 0) {
            perror("Cannot write into the file");
        }
    }

    free(line);

    if (nbytes < 0) {
        fclose(file);
        return 1;
    }

    return fclose(file);
}

int bulk_store_measures(struct timeval *results, size_t results_length, int server, int protocol,
                        long bytes_to_exchange, int repeat_number) {
    char *file_path = malloc(MAX_PATH_SIZE);
    char *loss_subfolder = malloc(MAX_PATH_SIZE);
    set_loss_subdirectory_name(list_losses_client, list_losses_server, loss_subfolder);

    snprintf(file_path, MAX_PATH_SIZE, "%s/bulk/%s/%s/%s/%d/%ld", output_directory,
             (protocol == TLS) ? "tls" : (protocol == MPTCPSEC ? "mptcpsec" : "mptcp"), loss_subfolder,
             server ? "server" : "client", repeat_number, bytes_to_exchange);

    printf("File path = %s\n", file_path);

    recursive_silent_mkdir(file_path);

    int err = store_time_measures(file_path, results, results_length);

    free(file_path);
    free(loss_subfolder);
    return err;
}

int bulk_attack_store_measures(struct timeval *results, size_t results_length, int server, int protocol,
                               long bytes_to_exchange) {
    char *file_path = malloc(MAX_PATH_SIZE);
    char *loss_subfolder = malloc(MAX_PATH_SIZE);
    set_loss_subdirectory_name(list_losses_client, list_losses_server, loss_subfolder);

    snprintf(file_path, MAX_PATH_SIZE, "%s/bulk_attack/%s/%s/%s/%ld", output_directory,
             (protocol == TLS) ? "tls" : (protocol == MPTCPSEC ? "mptcpsec" : "mptcp"), loss_subfolder,
             server ? "server" : "client", bytes_to_exchange);

    printf("File path = %s\n", file_path);

    recursive_silent_mkdir(file_path);

    int err = store_time_measures(file_path, results, results_length);

    free(file_path);
    free(loss_subfolder);
    return err;
}

void pingpong_output_capture_file(int client, int protocol, int iteration_number, long no_capture,
                                  long bytes_to_exchange, char *capture_path) {
    char *loss_subfolder = malloc(MAX_PATH_SIZE);
    set_loss_subdirectory_name(list_losses_client, list_losses_server, loss_subfolder);

    snprintf(capture_path, MAX_PATH_SIZE, "pingpong/%s/%s/%s/%s/%d/%ld.pcap",
                 (protocol == TLS) ? "tls" : (protocol == MPTCPSEC ? "mptcpsec" : "mptcp"), loss_subfolder,
                 client ? "client" : "server", no_capture ? "no_capture" : "capture", iteration_number,
                 bytes_to_exchange);

    printf("File partial path = %s\n", capture_path);

    recursive_silent_mkdir(capture_path);
    free(loss_subfolder);
}

int pingpong_store_measures(struct timeval *results, size_t results_length, int client, int protocol,
                            int iteration_number, long no_capture, long start, long step) {
    char *file_path = malloc(MAX_PATH_SIZE);
    char *loss_subfolder = malloc(MAX_PATH_SIZE);
    set_loss_subdirectory_name(list_losses_client, list_losses_server, loss_subfolder);

    int i;
    long bytes_to_exchange;
    for (i = 0, bytes_to_exchange = start; i < results_length; i += 3, bytes_to_exchange += step) {

        snprintf(file_path, MAX_PATH_SIZE, "%s/pingpong/%s/%s/%s/%s/%d/%ld", output_directory,
                 (protocol == TLS) ? "tls" : (protocol == MPTCPSEC ? "mptcpsec" : "mptcp"), loss_subfolder,
                 client ? "client" : "server", no_capture ? "no_capture" : "capture", iteration_number,
                 bytes_to_exchange);

        printf("File path = %s\n", file_path);

        recursive_silent_mkdir(file_path);

        int err = store_time_measures(file_path, results + i, 3);
        if (err) {
            free(file_path);
            free(loss_subfolder);
            return err;
        }
    }

    free(file_path);
    free(loss_subfolder);
    return 0;
}
