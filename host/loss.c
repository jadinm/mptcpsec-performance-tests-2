#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

#include "../include/list.h"
#include "../include/main.h"


int apply_loss_settings(struct list *loss_list) {

    struct list_header *current = NULL;
    struct list_header *next = NULL;

    losses_to_remove = 1;

    list_walk_safe(loss_list, current, next) {

        char *intf = ((struct loss_element *) current)->intf;
        char loss [10];
        snprintf(loss, 10, "%.2f%%", ((struct loss_element *) current)->loss);
        printf("intf = %s - loss = %s\n", intf, loss);
        char *capture_arguments[10] = {"tc", "qdisc", "add", "dev", intf, "root", "netem", "loss", loss, NULL};

        fflush(stdout);
        fflush(stderr);

        int pid = fork();
        int err;
        if (pid < 0) { /* Error */
            perror("fork() failed\n");
            return -1;
        } else if (pid == 0) { /* Child process */
            err = execvp("tc", capture_arguments);
            perror("execvp() failed");
            return err;
        }

        int status = 0;

        err = waitpid(pid, &status, 0);
        if (err != pid) {
            fprintf(stderr, "Setting loss rate couldn't be waited !\n");
            return err;
        } else if (!WIFEXITED(status)) {
            err = WEXITSTATUS(&status);
            fprintf(stderr, "Setting loss rate failed with error code %d. "
                    "Please check that the program is run with root privileges "
                    "and that interface names are correct.\n", err);;
            return err;
        }
    }

    return 0;
}

int remove_loss_settings(struct list *loss_list) {

    struct list_header *current = NULL;
    struct list_header *next = NULL;

    losses_to_remove = 0;

    list_walk_safe(loss_list, current, next) {

        char *intf = ((struct loss_element *) current)->intf;
        char *capture_arguments[8] = {"tc", "qdisc", "del", "dev", intf, "root", "netem", NULL};

        fflush(stdout);
        fflush(stderr);

        int pid = fork();
        int err;
        if (pid < 0) { /* Error */
            perror("fork() failed\n");
            return -1;
        } else if (pid == 0) { /* Child process */
            err = execvp("tc", capture_arguments);
            perror("execvp() failed");
            return err;
        }

        int status = 0;

        err = waitpid(pid, &status, 0);
        if (err != pid) {
            fprintf(stderr, "Setting loss rate couldn't be waited !\n");
            return err;
        } else if (!WIFEXITED(status)) {
            err = WEXITSTATUS(&status);
            fprintf(stderr, "Remove loss rules failed with error code %d. "
                    "Please check that the program is run with root privileges "
                    "and that interface names are correct.\n", err);;
            return err;
        }
    }

    return 0;
}

void set_loss_subdirectory_name(struct list *client_loss_list, struct list *server_loss_list, char *subdirectory) {

    struct list_header *current = NULL;
    struct list_header *next = NULL;

    char *client_prefix = "client_";
    char *server_prefix = "server_";
    char *default_name = "default";

    *subdirectory = '\0';

    size_t length = 1;

    if (!client_loss_list->length && !server_loss_list->length) {
        snprintf(subdirectory, MAX_PATH_SIZE, "%s", default_name);
        return;
    }

    if (client_loss_list->length) {
        snprintf(subdirectory, MAX_PATH_SIZE - length, "%s", client_prefix);
        length += strlen(subdirectory);
        subdirectory += strlen(subdirectory);

        list_walk_safe(client_loss_list, current, next) {

            snprintf(subdirectory, MAX_PATH_SIZE - length, "%s=%.2f%s", ((struct loss_element *) current)->intf,
                     ((struct loss_element *) current)->loss, next != &client_loss_list->list ? "-" : "");
            length += strlen(subdirectory);
            subdirectory += strlen(subdirectory);
        }
    }

    if (client_loss_list->length && server_loss_list->length) {
        snprintf(subdirectory, MAX_PATH_SIZE - length, "_");
        length += strlen(subdirectory);
        subdirectory += strlen(subdirectory);
    }

    if (server_loss_list->length) {
        snprintf(subdirectory, MAX_PATH_SIZE - length, "%s", server_prefix);
        length += strlen(subdirectory);
        subdirectory += strlen(subdirectory);

        list_walk_safe(server_loss_list, current, next) {

            snprintf(subdirectory, MAX_PATH_SIZE - length, "%s=%.2f%s", ((struct loss_element *) current)->intf,
                     ((struct loss_element *) current)->loss, next != &server_loss_list->list ? "-" : "");
            length += strlen(subdirectory);
            subdirectory += strlen(subdirectory);
        }
    }
}
