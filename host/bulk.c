#include <sys/types.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>

#include "../include/main.h"
#include "../include/data_transfer.h"

#define BUF_SIZE 4096


int bulk(int client, int protocol, const char *serverIP, const char *serverPort, long bytesToExchange,
         int repeat_number, int under_attack) {

    int sfd = -1;
    long i;
    BIO *bio = NULL;
    BIO *listen_bio = NULL;
    int listen_sfd = -1;

    char *buffer = calloc(1, BUF_SIZE);
    long block_number = bytesToExchange/BUF_SIZE + (bytesToExchange % BUF_SIZE != 0);
    struct timeval tval_array[block_number+1];
    struct timeval tval_result;

    int err = new_connection(client, protocol, serverIP, serverPort, &sfd, &bio, &listen_sfd, &listen_bio, buffer);
    if (err) {
        fprintf(stderr, "Connection establishment failed\n");
        return 1;
    }

    for (i=0; i < block_number; i++) {

        gettimeofday(&tval_array[i], NULL);
        if (client) {
            err = send_data(BUF_SIZE, sfd, bio, buffer, protocol);
            if (err) {
                fprintf(stderr, "Error while sending data\n");
                free_structures(listen_sfd, listen_bio, 1);
                goto error;
            }
        } else {
            err = recv_data(BUF_SIZE, sfd, bio, buffer, protocol);
            if (err) {
                fprintf(stderr, "Error while receiving data\n");
                free_structures(listen_sfd, listen_bio, 1);
                goto error;
            }
        }
    }

    gettimeofday(&tval_array[i], NULL);

    if (free_structures(sfd, bio, 0) || free_structures(listen_sfd, listen_bio, 1)) {
        free(buffer);
        goto error;
    }

    timersub(&tval_array[block_number], &tval_array[0], &tval_result);

    printf("Time elapsed for data transfer (negotiation time not counted): %ld.%06ld\n", (long int) tval_result.tv_sec,
           (long int) tval_result.tv_usec);

    if (!under_attack)
        bulk_store_measures(tval_array, (size_t) block_number + 1, !client, protocol, bytesToExchange, repeat_number);
    else
        bulk_attack_store_measures(tval_array, (size_t) block_number + 1, !client, protocol, bytesToExchange);

    free(buffer);
    return 0;

error:
    if (under_attack) {
        bulk_attack_store_measures(tval_array, (size_t) i + 1, !client, protocol, bytesToExchange);
    }
    return 1;
}

int client_bulk_loop(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                     long repeat) {
    int err = 0;
    char *ptr = NULL;
    int i = 0;

    char *current_server_port = malloc(strlen(serverPort) + 10);
    long current_port = strtoll(serverPort, &ptr, 10); // Should work because check before

    long transfer_size = 0;

    for (i = 0; i < repeat; i++) {
        printf("Iteration %d\n", i);
        for (transfer_size = start; transfer_size <= end;
             transfer_size += step, current_port += 1) {

            printf("Transfer size %ld - server port %ld\n", transfer_size, current_port);

            snprintf(current_server_port, strlen(serverPort) + 10, "%ld", current_port);

            err = bulk(1, protocol, serverIP, current_server_port, transfer_size, i, 0);
            if (err) {
                fprintf(stderr, "Transfer failed\n");
                return err;
            }

            sleep(2);
        }
    }

    return err;
}

int server_bulk_loop(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                     long repeat) {
    int err = 0;
    char *ptr = NULL;
    int i = 0;

    char *current_server_port = malloc(strlen(serverPort) + 10);
    long current_port = strtoll(serverPort, &ptr, 10); // Should work because check before

    long transfer_size = 0;

    for (i = 0; i < repeat; i++) {
        printf("Iteration %d\n", i);
        for (transfer_size = start; transfer_size <= end;
             transfer_size += step, current_port += 1) {

            printf("Transfer size %ld - server port %ld\n", transfer_size, current_port);

            snprintf(current_server_port, strlen(serverPort) + 10, "%ld", current_port);

            err = bulk(0, protocol, serverIP, current_server_port, transfer_size, i, 0);
            if (err) {
                fprintf(stderr, "Transfer failed\n");
                return err;
            }

            sleep(1);
        }
    }

    return err;
}

int bulk_attack(int client, int protocol, const char *serverIP, const char *serverPort,
                       long bytes_under_attack) {

    return bulk(client, protocol, serverIP, serverPort, bytes_under_attack, 0, 1);
}
