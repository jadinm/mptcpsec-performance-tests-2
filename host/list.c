#include <string.h>
#include <stdio.h>

#include "../include/list.h"


struct list *init_list() {

    struct list *list = (struct list *) malloc(sizeof(struct list *));
    if (!list)
        return NULL;

    list->list.prev = (struct list_header *) list;
    list->list.next = (struct list_header *) list;
    list->length = 0;

    return list;
}

void free_list(struct list *list) {

    if (!list)
        return;

    struct list_header *current = NULL;
    struct list_header *next = NULL;
    list_walk_safe(list, current, next) {
        free(current);
    }
    free(list);
}

void queue_tail(struct list *list, struct list_header *last) {

    if (!list || !last)
        return;

    last->prev = list->list.prev;
    last->next = &list->list;
    list->list.prev->next = last;
    list->list.prev = last;
    list->length++;
}

struct loss_element *init_loss_element(struct list *list, char *intf, float loss) {

    if (!list)
        return NULL;

    struct loss_element *elem = (struct loss_element *) malloc(sizeof(struct loss_element *));
    if (!elem)
        return NULL;

    int i;
    for (i=0; i < MAX_INTERFACE_SIZE && i < strlen(intf); i++) {
        (elem->intf)[i] = intf[i];
    }
    (elem->intf)[i] = '\0';

    queue_tail(list, (struct list_header *) elem);
    elem->loss = loss;

    return elem;
}
