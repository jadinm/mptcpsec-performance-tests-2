#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "../include/main.h"
#include "../include/connect.h"
#include "../include/listen.h"


int free_structures(int sfd, BIO *bio, int listen_only) {
    if (sfd >= 0 && close(sfd)) {
        return 1;
    } else if (bio) {
        SSL *ssl;
        BIO_get_ssl(bio, &ssl);
        if (!listen_only) {
            int err = SSL_shutdown(ssl);
            if (err) {
                ERR_print_errors_fp(stderr);
            }
            sfd = (int) BIO_get_fd(bio, &sfd);
            if (sfd > 0) {
                close(sfd);
            }
            SSL_free(ssl);
        } else {
            BIO_free_all(bio);
        }
        return 0;
    }
    return 0;
}

int send_data(long buff_size, int sfd, BIO *bio, char *buffer, int protocol) {

    ssize_t sent_length = 0;
    ssize_t current_sending_result = 0;
    ssize_t to_be_written;

    for (sent_length = 0; sent_length < buff_size; sent_length += current_sending_result) {
        to_be_written = buff_size - sent_length;

        if (protocol == MPTCPSEC || protocol == MPTCP) {
            current_sending_result = send(sfd, buffer + sent_length, (size_t) to_be_written, 0);
        } else {
            current_sending_result = BIO_write(bio, buffer + sent_length, (int) to_be_written);
        }
        if (current_sending_result < 0) {
            perror("Error writing to socket :\n");
            free_structures(sfd, bio, 0);
            free(buffer);
            return 1;
        }
    }

    return 0;
}

int recv_data(long buff_size, int sfd, BIO *bio, char *buffer, int protocol) {

    ssize_t recv_length = 0;
    ssize_t to_be_read;
    ssize_t current_receiving_result;

    for (recv_length = 0; recv_length < buff_size; recv_length += current_receiving_result) {
        to_be_read = buff_size - recv_length;

        if (protocol == MPTCPSEC || protocol == MPTCP) {
            current_receiving_result = recv(sfd, buffer + recv_length, (size_t) to_be_read, 0);
        } else {
            current_receiving_result = BIO_read(bio, buffer + recv_length, (int) to_be_read);
        }
        if (current_receiving_result < 0) {
            perror("Error writing to socket :\n");
            free_structures(sfd, bio, 0);
            free(buffer);
            return 1;
        }
    }

    return 0;
}

int new_connection(int client, int protocol, const char *serverIP, const char *serverPort, int *sfd, BIO **bio,
                   int *listen_sfd, BIO **listening_bio, char *buffer) {

    if (client) {
        if (protocol == MPTCPSEC) {
            *sfd = mptcpsec_connect(serverIP, serverPort);
            if (*sfd == -1) {
                fprintf(stderr, "Connection Failed !\n");
                free(buffer);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                return 1;
            }
        } else if (protocol == MPTCP) {
            *sfd = mptcp_connect(serverIP, serverPort);
            if (*sfd == -1) {
                fprintf(stderr, "Connection Failed !\n");
                free(buffer);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                return 1;
            }
        } else if (protocol == TLS) {
            *bio = tls_connect(serverIP, serverPort);
            if (!*bio) {
                fprintf(stderr, "Connection Failed !\n");
                free(buffer);
                return 1;
            }
        }
    } else if (*listen_sfd < 0 && !*listening_bio) { // Server setup
        if (protocol == MPTCPSEC) {
            *sfd = mptcpsec_listen(serverIP, serverPort, listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Listen Failed !\n");
                free(buffer);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                return 1;
            }
        } else if (protocol == MPTCP) {
            *sfd = mptcp_listen(serverIP, serverPort, listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Listen Failed !\n");
                free(buffer);
                return 1;
            } else if (*sfd == -2) {
                errno = EINVAL;
                perror("Not a valid IP address or port number :\n");
                free(buffer);
                return 1;
            }
        } else if (protocol == TLS) {
            *bio = tls_listen(serverIP, serverPort, listening_bio);
            if (!*bio) {
                fprintf(stderr, "Listen Failed !\n");
                free(buffer);
                return 1;
            }
        }
    } else {
        if (protocol == MPTCPSEC) {
            *sfd = mptcpsec_accept(*listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Accept Failed !\n");
                free_structures(*listen_sfd, *listening_bio, 1);
                free(buffer);
                return 1;
            }
        } else if (protocol == MPTCP) {
            *sfd = mptcp_accept(*listen_sfd);
            if (*sfd == -1) {
                fprintf(stderr, "Accept Failed !\n");
                free_structures(*listen_sfd, *listening_bio, 1);
                free(buffer);
                return 1;
            }
        } else if (protocol == TLS) {
            *bio = tls_accept(*listening_bio);
            if (!*bio) {
                fprintf(stderr, "Accept Failed !\n");
                free_structures(*listen_sfd, *listening_bio, 1);
                free(buffer);
                return 1;
            }
        }
    }

    return 0;
}
