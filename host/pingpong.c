#include <sys/types.h>
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdlib.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <wait.h>

#include "../include/main.h"
#include "../include/data_transfer.h"


static int start_capture(int protocol, int iteration_number, const char *serverPort, long buff_size, int client,
                         long no_capture) {
    char *serverPort_copy = malloc(strlen(serverPort));
    snprintf(serverPort_copy, 100, "%s", serverPort);
    char *capture_path = capture_full_path();
    char *output_capture = malloc(MAX_PATH_SIZE);
    pingpong_output_capture_file(client, protocol, iteration_number, no_capture, buff_size, output_capture);
    char *capture_arguments[5] = {capture_path, serverPort_copy, output_directory, output_capture, NULL};

    fflush(stdout);
    fflush(stderr);

    int pid = fork();
    if (pid < 0) { /* Error */
        perror("fork() failed : \n");
        return -1;
    } else if (pid == 0) { /* Child process */
        int err = execvp(capture_path, capture_arguments);
        perror("execvp() failed");
        fprintf(stderr, "Error code %d\n", err);
        return err;
    }

    free(output_capture);
    return pid;
}

static int stop_capture(int pid) {

    if (kill(pid, SIGINT)) {
        perror("Fail to close the process :");
        return 1;
    }
    return 0;
}

static int pingpong_behaviour(int client, int protocol, int sfd, BIO *bio, int listen_sfd, BIO *listening_bio,
                              long loop_iter, char *buffer, long buff_size, struct timeval *tval_array) {

    if (client) {
        gettimeofday(&tval_array[loop_iter], NULL); // Take time before sending

        int err = send_data(buff_size, sfd, bio, buffer, protocol);
        if (err > 0) {
            fprintf(stderr, "Sending data failed !\n");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        }

        gettimeofday(&tval_array[loop_iter + 1], NULL); // Take time before receiving

        err = recv_data(buff_size, sfd, bio, buffer, protocol);
        if (err > 0) {
            fprintf(stderr, "Receiving data failed !\n");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        }

        gettimeofday(&tval_array[loop_iter + 2], NULL); // Take time after operations
    } else {
        gettimeofday(&tval_array[loop_iter], NULL); // Take time before receiving

        int err = recv_data(buff_size, sfd, bio, buffer, protocol);
        if (err > 0) {
            fprintf(stderr, "Receiving data failed !\n");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        }

        gettimeofday(&tval_array[loop_iter + 1], NULL); // Take time before sending

        err = send_data(buff_size, sfd, bio, buffer, protocol);
        if (err > 0) {
            fprintf(stderr, "Sending data failed !\n");
            free_structures(listen_sfd, listening_bio, 1);
            return 1;
        }

        gettimeofday(&tval_array[loop_iter + 2], NULL); // Take time after operations
    }

    return 0;
}

static int pingpong(int client, int protocol, const char *serverIP, const char *serverPort, int iteration_number,
                    long noCapture, long start, long step, long end) {

    int sfd = -1;
    int listen_sfd = -1;
    int pid = -1;
    BIO *bio = NULL;
    BIO *listening_bio = NULL;

    long max_limit = end;
    char *buffer = calloc(1, (size_t) max_limit);

    long blocks = max_limit / step + (max_limit % step != 0);
    struct timeval tval_array[blocks*3];
    struct timeval tval_result;

    long i = 0;
    long buff_size = start;
    do {

        sleep(2); // Sleep in order to prevent previous connections from interfering inside the capture

        if (pid > 0 && stop_capture(pid)) {
            fprintf(stderr, "Stopping capture has failed\n");
            return 1;
        }

        sleep(1);

        /* New Connection */

        if (new_connection(client, protocol, serverIP, serverPort, &sfd, &bio, &listen_sfd,
                           &listening_bio, buffer)) {
            if (i != 0)
                pingpong_store_measures(tval_array, (size_t) i, client, protocol, iteration_number,
                                        noCapture, start, step);
            fprintf(stderr, "Cannot establish/receive the connection\n");
            return 1;
        }

        /* Start capture if we are on server side */
        if (!client && !noCapture) {
            pid = start_capture(protocol, iteration_number, serverPort, buff_size, client, noCapture);
            if (pid < 0) {
                fprintf(stderr, "Error launching the capture\n");
                return 1;
            }
        }

        sleep(1); // To be sure that capture is launched and that crypto data structures are initialized for MPTCPsec

        int err = pingpong_behaviour(client, protocol, sfd, bio, listen_sfd, listening_bio, i, buffer, buff_size,
                                     tval_array);
        if (err > 0) {
            return 1;
        }

        /* Close connection */

        if (free_structures(sfd, bio, 0)) {
            perror("Error closing connection !\n");
            free_structures(listen_sfd, listening_bio, 1);
            free(buffer);
            return 1;
        }

        timersub(&tval_array[i+2], &tval_array[i], &tval_result);

        printf("Time elapsed for data transfer (negotiation time and closing time not counted): %ld.%06ld\n",
               (long int) tval_result.tv_sec, (long int) tval_result.tv_usec);

        /* Variable updates */

        buff_size += step;
        i += 3;

    } while (buff_size <= max_limit);

    if (free_structures(listen_sfd, listening_bio, 1)) {
        fprintf(stderr, "Problem freeing the listening socket\n");
        free(buffer);
        return 1;
    }

    pingpong_store_measures(tval_array, (size_t) i, client, protocol, iteration_number, noCapture, start, step);

    free(buffer);

    if (pid > 0 && stop_capture(pid)) {
        fprintf(stderr, "Stopping capture has failed\n");
        return 1;
    }

    sleep(1);

    return 0;
}

int client_pingpong(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                    long iterations, long no_capture) {

    int err = 0;
    int i = 0;
    char *ptr = NULL;

    char *current_server_port = malloc(strlen(serverPort) + 10);
    long current_port = strtoll(serverPort, &ptr, 10); // Should work because check before

    for (i=0; i < iterations; i++, current_port++) {
        printf("Iteration number %d\n", i);
        snprintf(current_server_port, strlen(serverPort) + 10, "%ld", current_port);
        err = pingpong(1, protocol, serverIP, current_server_port, i, no_capture, start, step, end);
        if (err)
            return err;
        sleep(2);
    }

    return err;
}

int server_pingpong(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                    long iterations, long no_capture) {

    int err = 0;
    int i = 0;
    char *ptr = NULL;

    char *current_server_port = malloc(strlen(serverPort) + 10);
    long current_port = strtoll(serverPort, &ptr, 10); // Should work because check before

    for (i=0; i < iterations; i++, current_port++) {
        printf("Iteration number %d\n", i);
        snprintf(current_server_port, strlen(serverPort) + 10, "%ld", current_port);
        err = pingpong(0, protocol, serverIP, current_server_port, i, no_capture, start, step, end);
        if (err)
            return err;
        sleep(1);
    }

    return 0;
}
