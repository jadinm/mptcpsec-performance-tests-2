import subprocess
import re


AF_LINK = 18
AF_INET6 = 30
AF_INET = 2


class Iface:

    def __init__(self, iface, output=None):

        self.iface = iface

        if output is None:
            output = subprocess.check_output(["ifconfig"], universal_newlines=True)

        output_lines = str(output).split("\n")
        start_line = -1
        i = 0
        for line in output_lines:
            if len(line) > 0 and line[0] != "\n" and line[0] != " ":
                index = line.find(iface)
                if index == 0:
                    start_line = i
                    break
            i += 1

        if start_line < 0:
            raise Exception("The interface was not found !")

        self.links = {}

        for i in range(start_line + 1, len(output_lines)):
            line = output_lines[i]
            if len(line) > 0 and line[0] != "\n" and line[0] != " ":
                break  # Another interface
            if line.find("inet ") >= 0:
                self.process_af_inet(line)
            elif line.find("inet6 ") >= 0:
                self.process_af_inet6(line)
            elif line.find("ether ") >= 0:
                self.process_af_link(line)
            else:  # End of the interesting part
                break

    def process_af_inet(self, line):

        ipv4_address_reg = r"((?:[0-9]{1,3}\.){3}[0-9]{1,3})"

        groups = re.search(r"inet\b(?:addr:)|(?:\b)" + ipv4_address_reg, line)
        if groups is None:
            raise Exception("Line does not match a regular inet line !")
        ipv4_address = groups.groups(1)[0]

        groups = re.search(r"(?:netmask\b)|(?:Mask:)" + ipv4_address_reg, line)
        netmask = groups.groups(1)[0] if groups is not None else None

        groups = re.search(r"(?:broadcast\b)|(?:Bcast:)" + ipv4_address_reg, line)
        broadcast = groups.groups(1)[0] if groups is not None else None

        link = {"addr": ipv4_address, "netmask": netmask, "broadcast": broadcast}

        if AF_INET not in self.links:
            self.links[AF_INET] = [link]
        else:
            self.links[AF_INET].append(link)

    def process_af_inet6(self, line):

        pass  # TODO

    def process_af_link(self, line):

        mac_address_reg = r"((?:[0-9A-Fa-f]{2}[:-]){5}(?:[0-9A-Fa-f]{2}))"

        groups = re.search(mac_address_reg, line)

        if groups is None:
            raise Exception("Line does not match a regular ether line !")

        mac_address = groups.groups(1)[0]

        link = {"addr": mac_address}

        if AF_LINK not in self.links:
            self.links[AF_LINK] = [link]
        else:
            self.links[AF_LINK].append(link)

    def to_dictionary(self):

        return self.links

    def __str__(self):
        return self.iface


class IfaceList(list):

    def __iter__(self):
        iterable = [self[i].links for i in range(len(self))]  # Get the dictionary instead of the objects
        return iter(iterable)


def interfaces(iface=None):

    output = subprocess.check_output(["ifconfig"], universal_newlines=True)

    if iface is not None:
        return Iface(iface, output).to_dictionary()

    list_ifaces = IfaceList()

    for line in str(output).split("\n"):
        if len(line) > 0 and line[0] != "\n" and line[0] != " ":
            index = line.find(" ")
            if index < 0:
                raise Exception("Not a correct output of ifconfig")
            iface = line[:index]
            list_ifaces.append(Iface(iface, output))

    return list_ifaces
