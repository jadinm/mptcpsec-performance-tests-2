from abc import ABCMeta, abstractmethod
from subprocess import check_call

from scapy.all import *

from packetFormat import SYN_FLAG, ACK_FLAG, RST_FLAG, FIN_FLAG
from sysctl import get_value_from_sysctl, set_sysctl_value, IPV4_FORWARD, IPV6_FORWARD


class Proxy:
    __metaclass__ = ABCMeta

    def __init__(self, proxy_ip_addresses, proxy_mac_addresses):
        self.proxy_ip_addresses = proxy_ip_addresses
        for ip in proxy_ip_addresses:
            # We prevent the Proxy to send a RST to an incoming SYN
            check_call(["iptables", "-A", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST", "-s", ip, "-j", "DROP"])

        self.proxy_mac_addresses = proxy_mac_addresses

        self._current_packet = None
        # Dictionary {(client_ip, server_ip, client_port, server_port): (fully_established, closing), ...}
        self._subflows = {}

        self._initial_subflows_established = False

        self._stop_proxy = False

        self.sock = None

        self.ipv4_old_value = get_value_from_sysctl(IPV4_FORWARD)
        self.ipv6_old_value = get_value_from_sysctl(IPV6_FORWARD)

    def _get_subflow_key(self, src_ip, dst_ip, src_port, dst_port):
        if (src_ip, dst_ip, src_port, dst_port) in self._subflows:
            return src_ip, dst_ip, src_port, dst_port
        elif (dst_ip, src_ip, dst_port, src_port) in self._subflows:
            return dst_ip, src_ip, dst_port, src_port
        return src_ip, dst_ip, src_port, dst_port

    def keep_going(self, pkt):
        key = self._get_subflow_key(pkt[IP].src, pkt[IP].dst, pkt[TCP].sport, pkt[TCP].dport)
        if pkt[TCP].flags & SYN_FLAG != 0:
            if key in self._subflows:
                self._subflows[key] = (True, False)
                self._initial_subflows_established = True
            else:
                self._subflows[key] = (False, False)
        elif pkt[TCP].flags & RST_FLAG != 0:
            if key in self._subflows:
                del self._subflows[key]
        elif pkt[TCP].flags & FIN_FLAG != 0:
            self._subflows = {}  # End of the connection
        return not self._stop_proxy and (len(self._subflows) > 0 or not self._initial_subflows_established)

    @abstractmethod
    def is_modification_needed(self, pkt):
        return False

    @abstractmethod
    def modify_and_send(self, pkt):
        pass

    def handle(self, pkt):
        self._current_packet = pkt
        if self.is_modification_needed(pkt):
            self.modify_and_send(pkt)
        else:  # Simple forwarding (we remove datalink layer)
            tcp = TCP(sport=pkt[TCP].sport, dport=pkt[TCP].dport, seq=pkt[TCP].seq, ack=pkt[TCP].ack,
                      flags=pkt[TCP].flags, window=pkt[TCP].window, options=pkt[TCP].options)
            tcp.payload = pkt[TCP].payload
            ip = IP(src=pkt[IP].src, dst=pkt[IP].dst)
            self.sock.send(ip/tcp)

    def filter_str(self):
        s = "tcp"
        for mac in self.proxy_mac_addresses:
            s += " && !(ether src " + mac + ")"
        s += " && ("
        i = 1
        for mac in self.proxy_mac_addresses:
            s += "(ether dst " + mac + ")"
            if i < len(self.proxy_mac_addresses):
                s += " || "
            i += 1
        s += ")"
        return s

    def run(self):
        self.ipv4_old_value = get_value_from_sysctl(IPV4_FORWARD)
        self.ipv6_old_value = get_value_from_sysctl(IPV6_FORWARD)
        set_sysctl_value(IPV4_FORWARD, "0")
        set_sysctl_value(IPV6_FORWARD, "0")
        self.sock = conf.L3socket()
        sniff(prn=lambda new_pkt: self.handle(new_pkt), filter=self.filter_str(), store=0,
              stop_filter=lambda new_pkt: not self.keep_going(new_pkt))
        self.shutdown()

    def shutdown(self):
        # We allow again the host to send a RST to packets and to forward packets
        set_sysctl_value(IPV4_FORWARD, self.ipv4_old_value)
        set_sysctl_value(IPV6_FORWARD, self.ipv6_old_value)
        for ip in self.proxy_ip_addresses:
            check_call(["iptables", "-D", "OUTPUT", "-p", "tcp", "--tcp-flags", "ALL", "RST", "-s", ip, "-j", "DROP"])
