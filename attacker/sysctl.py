from subprocess import check_output, STDOUT, CalledProcessError


IPV4_FORWARD = "net.ipv4.ip_forward"
IPV6_FORWARD = "net.ipv6.conf.all.forwarding"


def get_value_from_sysctl(variable_name):

    try:
        return check_output(["sysctl", "-n", "-b", variable_name], stderr=STDOUT, universal_newlines=True)

    except CalledProcessError as e:
        print("CalledProcessError message=" + str(e))


def set_sysctl_value(variable_name, value):

    try:
        check_output(["sysctl", "-w", variable_name + "=" + value], stderr=STDOUT, universal_newlines=True)

    except CalledProcessError as e:
        print("CalledProcessError message=" + str(e))
