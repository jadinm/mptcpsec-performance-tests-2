#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include "../include/main.h"
#include "../include/bulk.h"
#include "../include/pingpong.h"
#include "../include/loss.h"

void help() {

    fflush(stderr);
    fflush(stdout);
    printf("Usage : ./server serverIP serverPort protocol [-a bytes] [-b start:step:end] [-p start:step:end] "
                   "[-r number_of_repetition] [-c] \n"
                   START_HELP_LINE
                   "[-l intf1=percentage  [-l intf2=percentage ...]] [-L intf1=percentage [-L intf2=percentage ...]] \n"
                   START_HELP_LINE
                   "[-d intf1=delay [-d intf2=delay ...]] [-D intf1=delay [-D intf2=delay ...]] [-o directory] [-h]\n");
    printf("The same arguments must be used on server and client sides (except the output directory, of course)\n");
    printf("Options and arguments:\n");
    printf("\tprotocol \t\t\t\t\tUse one of the following protocols: \"tls\", \"mptcpsec\" or \"mptcp\"\n");
    printf("\t-a bytes \t\t\t\t\tFirst experiment if mentioned, try to exchange \"bytes\" B when under attack\n");
    printf("\t-b start:step:end \t\t\tMake bulk transfers from the client to the server of with different data size\n");
    printf("\t-p start:step:end \t\t\tMake ping-pong transfers between the client to the server of with different data"
                   " size\n");
    printf("\t-r number_of_repetition \tnumber of times that the measures should be rerun\n");
    printf("\t-c \t\t\t\t\t\t\tWhether there must be a capture on the server side or not\n");
    printf("\t-l intf=loss_percentage \tSet the loss percentage for this interface (client side)\n");
    printf("\t-L intf=loss_percentage \tSet the loss percentage for this interface (server side)\n");
    printf("\t-d intf=delay \t\t\t\tSet the delay (ms) for this interface (client side)\n");
    printf("\t-D intf=delay \t\t\t\tSet the delay (ms) for this interface (server side)\n");
    printf("\t-o directory \t\t\t\tRoot directory of the output files (if omitted, the current directory will be used)\n");
    printf("\t-h \t\t\t\t\t\t\tPrint this message\n");
}

int main(int argc, char *argv []) {

    int err = 0;

    int protocol;

    long bytes_under_attack = 0;

    long start_bulk = -1;
    long step_bulk = -1;
    long end_bulk = -1;

    long start_pingpong = -1;
    long step_pingpong = -1;
    long end_pingpong = -1;

    long iterations = 1;

    char *serverIP = NULL;
    char *serverPort = NULL;

    int no_capture = 1;

    /* Argument parsing */

    err = argument_parsing(argc, argv, &no_capture, &bytes_under_attack, &start_bulk, &step_bulk, &end_bulk,
                           &start_pingpong, &step_pingpong, &end_pingpong, &iterations, &serverIP, &serverPort,
                           &protocol);
    if (err) {
        help();
        if (err > 0)
            goto error;
        else
            goto success;
    }

    /* Set the losses and delays */

    // TODO delays
    err = apply_loss_settings(list_losses_server);
    if (err) {
        goto error;
    }

    /* Launch tests */

    int test_started = 0;

    if (bytes_under_attack > 0) {
        test_started += 1;
        printf("Start of the transfer under attack\n");
        err = bulk_attack(0, protocol, serverIP, serverPort, bytes_under_attack);
        if (err > 0) {
            printf("Transfer has failed (but this is not due to the attack) !\n");
            goto error;
        } else if (err < 0) {
            if (protocol == MPTCPSEC) {
                fprintf(stderr, "The attack has succeeded in MPTCPsec case !\n");
                goto error;
            } else {
                printf("Transfer has failed => the attack was successful !\n");
            }
        } else {
            printf("Transfer has succeeded despite the attack !\n");
        }
        /* Sleep so that the server can start listening before restart */
        sleep(30);
    }

    if (start_bulk >= 0) {
        test_started += 1;
        printf("Start of the bulk transfers\n");
        err = server_bulk_loop(protocol, serverIP, serverPort, start_bulk, step_bulk, end_bulk,
                               iterations);
        if (err) {
            fprintf(stderr, "The performance test failed to execute\n");
            goto error;
        }
        /* Sleep before next test to give time for established connections to be closed */
        sleep(2);
    }

    if (start_pingpong >= 0) {
        test_started += 1;
        printf("Start of the ping-pong transfers\n");
        err = server_pingpong(protocol, serverIP, serverPort, start_pingpong, step_pingpong, end_pingpong, iterations,
                              no_capture);
        if (err) {
            fprintf(stderr, "The performance test failed to execute\n");
            goto error;
        }
        /* Sleep before next test to give time for established connections to be closed */
        sleep(3);
    }

    if (!test_started) {
        errno = EINVAL;
        perror("No test selected :\n");
        help();
        goto error;
    }

success:
    err = EXIT_SUCCESS;
free:
    if (losses_to_remove)
        remove_loss_settings(list_losses_server);
    free_list(list_losses_server);
    free_list(list_losses_client);
    exit(err);
error:
    err = EXIT_FAILURE;
    goto free;
}
