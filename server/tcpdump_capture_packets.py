#!/usr/bin/env python3

import sys
import os
import subprocess
import signal
import shutil


if len(sys.argv) < 3:
    print("Not enough arguments")
else:
    print(str(sys.argv))

portNumber = sys.argv[1]
output_directory = sys.argv[2]
capture_file = sys.argv[3]

# Initialize ramdisk if needed
ramdisk = "virtuelram"
init_file = "init"
os.makedirs(ramdisk, mode=0o777, exist_ok=True)
if not os.path.exists(os.path.join(ramdisk, init_file)):  # Ramdisk not mounted

    process = subprocess.Popen(["mount", "-t", "tmpfs", "-o", "size=50M", "tmpfs", ramdisk],
                               universal_newlines=True)
    process.wait()
    if process.returncode != 0:
        print("Initialize ramdisk has failed !")
        sys.exit(1)

    open(os.path.join(ramdisk, init_file), "w").close()


# Launch capture
os.makedirs(os.path.dirname(os.path.os.path.join(ramdisk, capture_file)), mode=0o777, exist_ok=True)
cmd = ["tcpdump", "-w", os.path.join(ramdisk, capture_file), "-i", "any", "tcp", "and", "port", portNumber]
process = subprocess.Popen(cmd, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def signal_handler(sig, frame):
    print('tcpdump is going to be closed')
    process.terminate()
    process.communicate()
    process.wait()
    if process.returncode != 0:
        print("tcpdump has failed... Please check that the program was run as the right to launch a capture")
        sys.exit(1)
    os.makedirs(os.path.dirname(os.path.os.path.join(output_directory, capture_file)), mode=0o777, exist_ok=True)
    shutil.move(os.path.join(ramdisk, capture_file), os.path.join(output_directory, capture_file))
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.pause()
