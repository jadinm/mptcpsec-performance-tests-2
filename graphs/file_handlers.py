from abc import ABC, abstractmethod

from scapy.all import *


TLS = "tls"
MPTCPSEC = "mptcpsec"
MPTCP = "mptcp"

CLIENT = "client"
SERVER = "server"

NO_LOSSES_STR = "default"


class DataFileHandler(ABC):

    def __init__(self, graph_subdirectory, sub_file_path):

        self.graph_subdirectory = graph_subdirectory
        self.file_path = sub_file_path
        self.full_path = os.path.join(graph_subdirectory, sub_file_path)

        self.is_server_file = self.file_path.find(SERVER) >= 0
        self.no_capture = self.file_path.find("capture") < 0
        if self.file_path.find(TLS) >= 0:
            self.protocol = TLS
        elif self.file_path.find(MPTCPSEC) >= 0:
            self.protocol = MPTCPSEC
        elif self.file_path.find(MPTCP) >= 0:
            self.protocol = MPTCP

        start_losses_str = len(graph_subdirectory) + len(self.protocol) + 2
        self.losses_str = self.file_path[start_losses_str:self.file_path.find("/", start_losses_str)]

    @abstractmethod
    def interpret_file(self):
        pass


class TimeFileHandler(DataFileHandler):

    def __init__(self, graph_subdirectory, sub_file_path):

        super(TimeFileHandler, self).__init__(graph_subdirectory, sub_file_path)

        self.total_duration = -1.0
        self.byte_transferred = int(os.path.basename(sub_file_path))
        try:
            self.iteration = int(os.path.basename(os.path.dirname(sub_file_path)))
        except ValueError:
            self.iteration = 0

    @staticmethod
    def convert_to_microseconds(number_str):

        seconds = number_str[:number_str.find(".")]
        if len(number_str) > len(seconds):
            microseconds = number_str[number_str.find(".")+1:]
        else:
            microseconds = "0"

        return float(int(seconds) * 10**6 + int(microseconds))

    def interpret_file(self):

        times = []

        self.total_duration = 0.0
        start_time = -1.0
        with open(self.full_path, "r") as file_obj:
            for line in file_obj:
                if start_time < 0 and len(line) > 1:
                    start_time = (self.convert_to_microseconds(line[:-1]))
                if len(line) > 1:
                    times.append((self.convert_to_microseconds(line[:-1]) - start_time) / 1000.0)
        self.total_duration = times[-1] if len(times) > 0 else 0.0

        return times


class CaptureFileHandler(DataFileHandler):

    def __init__(self, graph_subdirectory, sub_file_path):

        super(CaptureFileHandler, self).__init__(graph_subdirectory, sub_file_path)

        extension_start_index = os.path.basename().rfind(".")
        self.byte_transferred = int(os.path.basename()[:extension_start_index])
        self.iteration = int(os.path.dirname(sub_file_path))
        self.no_capture = False

    def interpret_file(self):
        pass  # TODO -> compute current throughput (will be useful for attack graphs)


class PingPongCaptureFileHandler(CaptureFileHandler):

    def __init__(self, graph_subdirectory, sub_file_path):

        super(PingPongCaptureFileHandler, self).__init__(graph_subdirectory, sub_file_path)

        self.ping_pong_time = -1

    def interpret_file(self):

        if self.ping_pong_time >= 0:
            return self.ping_pong_time

        super(PingPongCaptureFileHandler, self).interpret_file()

        protocol_close_data_size = 0
        if self.protocol == TLS:
            # (content type, version and length) + explicit nonce + tag + alert
            protocol_close_data_size = 5 + 8 + 16 + 2

        ping_time = -1
        pong_time = -1
        last_pong_time = None
        packet_list = rdpcap(self.full_path)

        for packet in packet_list:

            if TCP in packet and len(packet[TCP].payload) > 0:
                if ping_time < 0:
                    ping_time = packet.time
                    pong_src_address = packet[IP].dst
                elif protocol_close_data_size != 0 and len(packet[TCP].payload) == protocol_close_data_size:
                    pong_time = packet.time
                    break
                elif pong_src_address is not None and pong_src_address == packet[IP].src:
                    last_pong_time = packet.time

        if last_pong_time is not None:
            pong_time = last_pong_time

        if ping_time > 0 and pong_time > 0:
            self.ping_pong_time = (pong_time - ping_time) * 1000

        return self.ping_pong_time
