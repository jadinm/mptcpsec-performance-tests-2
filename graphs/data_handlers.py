import os
import matplotlib.pyplot as plt
from matplotlib import rcParams
from abc import ABC, abstractmethod

from file_handlers import TLS, MPTCP, MPTCPSEC, TimeFileHandler, PingPongCaptureFileHandler, CLIENT, SERVER,\
    NO_LOSSES_STR

RESULTS_SUB_FOLDER = "results"

colors_dict = {MPTCPSEC: "red", MPTCP: "black", TLS: "#ffcc00"}


class DataHandler(ABC):

    def __init__(self, protocol_used, input_sub_folder, graph_sub_folder, method):

        self.protocol_used = protocol_used
        self.graph_sub_folder = graph_sub_folder
        self.input_sub_folder = input_sub_folder
        self.method = method
        self.file_handlers = []

    @staticmethod
    def convert_bytes_to_correct_unit(bytes_amount):

        bytes_amount = int(bytes_amount)
        if bytes_amount >= (10 ** 9):
            return (10 ** 9), "GB"
        elif bytes_amount >= (10 ** 6):
            return (10 ** 6), "MB"
        elif bytes_amount >= (10 ** 3):
            return (10 ** 3), "kB"
        else:
            return 1, "B"

    @abstractmethod
    def dispatch_file_handler(self, graph_directory, file_name):
        pass

    def __str__(self):
        return self.method

    def handle(self, script_path):

        graph_directory = os.path.join(os.path.dirname(script_path), self.graph_sub_folder)

        # Get all file handlers
        for (dir_path, dir_names, file_names) in os.walk(graph_directory):
            if dir_path.find("results") < 0:
                print("dir_path = " + dir_path)
                for file_name in file_names:
                    file_handler = self.dispatch_file_handler(graph_directory,
                                                              os.path.relpath(os.path.join(dir_path, file_name),
                                                                              graph_directory))
                    if file_handler is not None:
                        file_handler.interpret_file()  # Compute statistics on the file
                        self.file_handlers.append(file_handler)

        print("End of Simple DataHandler handle -> now start the real thing !")


class TimeMeasureHandler(DataHandler):

    def __init__(self, protocol_used, input_sub_folder, graph_sub_folder, method):

        super(TimeMeasureHandler, self).__init__(protocol_used, input_sub_folder, graph_sub_folder, method)

        self.time_dictionaries = {}
        for protocol in self.protocol_used:
            self.time_dictionaries[protocol] = {CLIENT: {}, SERVER: {}}

    @abstractmethod
    def dispatch_file_handler(self, graph_directory, file_name):
        pass


class CaptureHandler(DataHandler):

    def __init__(self, protocol_used, input_sub_folder, graph_sub_folder, method):

        super(CaptureHandler, self).__init__(protocol_used, input_sub_folder, graph_sub_folder, method)

        self.capture_dictionaries = {}
        for protocol in self.protocol_used:
            self.capture_dictionaries[protocol] = {CLIENT: {}, SERVER: {}}

    @abstractmethod
    def dispatch_file_handler(self, graph_directory, file_name):
        pass


class ThroughputMeasureHandler(TimeMeasureHandler):

    def __init__(self):

        super(ThroughputMeasureHandler, self).__init__(protocol_used=[TLS, MPTCPSEC, MPTCP], input_sub_folder="bulk",
                                                       graph_sub_folder="bulk",
                                                       method="Throughput measure")

    @staticmethod
    def compute_throughput(bytes_transferred, list_of_file_handlers):

        total_time = 0.0
        iterations = 0.0
        for file_handler in list_of_file_handlers:
            if file_handler.total_duration > 0:
                total_time += file_handler.total_duration
                iterations += 1
        return (bytes_transferred * iterations * 1000.0) / total_time

    def dispatch_file_handler(self, graph_directory, file_name):
        return TimeFileHandler(graph_directory, file_name)

    def handle(self, script_path):

        # Create file handlers
        super(ThroughputMeasureHandler, self).handle(script_path)

        graph_directory = os.path.join(os.path.dirname(script_path), self.graph_sub_folder)

        # Sort file handlers
        for file_handler in self.file_handlers:

            bytes_transferred = file_handler.byte_transferred
            if bytes_transferred <= 0 or file_handler.protocol not in self.time_dictionaries:
                continue

            dictionary = self.time_dictionaries[file_handler.protocol][
                SERVER if file_handler.is_server_file else CLIENT]
            if file_handler.losses_str not in dictionary:
                dictionary[file_handler.losses_str] = {file_handler.byte_transferred: [file_handler]}
            elif file_handler.byte_transferred not in dictionary[file_handler.losses_str]:
                dictionary[file_handler.losses_str][file_handler.byte_transferred] = [file_handler]
            else:
                dictionary[file_handler.losses_str][file_handler.byte_transferred].append(file_handler)

        # Interpret data
        for losses_str, value in self.time_dictionaries[self.protocol_used[0]][SERVER].items():

            tls_client_throughput_list_temp = []
            mptcpsec_client_throughput_list_temp = []
            mptcp_client_throughput_list_temp = []
            tls_server_throughput_list_temp = []
            mptcpsec_server_throughput_list_temp = []
            mptcp_server_throughput_list_temp = []

            for byte_transferred, file_handler_list in value.items():

                # Check that the file_handler is available
                available = True
                for protocol in self.protocol_used:
                    if byte_transferred not in self.time_dictionaries[protocol][SERVER][losses_str] or \
                       byte_transferred not in self.time_dictionaries[protocol][CLIENT][losses_str]:
                        available = False
                        break
                if not available:
                    continue

                # Collect data for throughput measures
                if byte_transferred >= 50000000:
                    print("tls_client_throughput_list_temp = " + str(tls_client_throughput_list_temp))
                    tls_client_throughput_list_temp.append((byte_transferred,
                                                            self.compute_throughput(byte_transferred,
                                                                                    self.time_dictionaries[TLS][CLIENT][
                                                                                        losses_str][byte_transferred])))
                    mptcpsec_client_throughput_list_temp.append((byte_transferred,
                                                                 self.compute_throughput(byte_transferred,
                                                                                         self.time_dictionaries[
                                                                                             MPTCPSEC][CLIENT][
                                                                                             losses_str][
                                                                                             byte_transferred])))
                    mptcp_client_throughput_list_temp.append((byte_transferred,
                                                              self.compute_throughput(byte_transferred,
                                                                                      self.time_dictionaries[MPTCP][
                                                                                          CLIENT][losses_str][
                                                                                          byte_transferred])))
                    tls_server_throughput_list_temp.append((byte_transferred,
                                                            self.compute_throughput(byte_transferred,
                                                                                    self.time_dictionaries[TLS][SERVER][
                                                                                        losses_str][byte_transferred])))
                    mptcpsec_server_throughput_list_temp.append((byte_transferred,
                                                                 self.compute_throughput(byte_transferred,
                                                                                         self.time_dictionaries[
                                                                                             MPTCPSEC][SERVER][
                                                                                             losses_str][
                                                                                             byte_transferred])))
                    mptcp_server_throughput_list_temp.append((byte_transferred,
                                                              self.compute_throughput(byte_transferred,
                                                                                      self.time_dictionaries[MPTCP][
                                                                                          SERVER][losses_str][
                                                                                          byte_transferred])))

                # Get data for graph of the data transferred over the time
                tls_client_times = self.time_dictionaries[TLS][CLIENT][losses_str][byte_transferred][0].interpret_file()
                tls_server_times = self.time_dictionaries[TLS][SERVER][losses_str][byte_transferred][0].interpret_file()
                mptcpsec_client_times = self.time_dictionaries[MPTCPSEC][CLIENT][losses_str][byte_transferred][0]\
                    .interpret_file()
                mptcpsec_server_times = self.time_dictionaries[MPTCPSEC][SERVER][losses_str][byte_transferred][0]\
                    .interpret_file()
                mptcp_client_times = self.time_dictionaries[MPTCP][CLIENT][losses_str][byte_transferred][0]\
                    .interpret_file()
                mptcp_server_times = self.time_dictionaries[MPTCP][SERVER][losses_str][byte_transferred][0]\
                    .interpret_file()

                # Build graphs of data transferred over the time
                graph_client_comparison_title = "Transferred data from client perspective\n"
                graph_client_comparison_file_name = "client_throughput_measure_" + losses_str + "_" + \
                                                    str(byte_transferred) + ".svg"
                graph_server_comparison_title = "Transferred Data from server perspective\n"
                graph_server_comparison_file_name = "server_throughput_measure_" + losses_str + "_" + \
                                                    str(byte_transferred) + ".svg"

                self.plot_data_transferred_comparison(byte_transferred, tls_client_times, mptcpsec_client_times,
                                                      mptcp_client_times, title=graph_client_comparison_title,
                                                      graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                  graph_client_comparison_file_name))
                self.plot_data_transferred_comparison(byte_transferred, tls_server_times, mptcpsec_server_times,
                                                      mptcp_server_times, title=graph_server_comparison_title,
                                                      graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                  graph_server_comparison_file_name))

            # Parse data in order to obtain the throughput for each transfer size
            print("Before sorting - tls_client_throughput_list_temp = " + str(tls_client_throughput_list_temp))
            tls_client_throughput_list_temp = sorted(tls_client_throughput_list_temp, key=lambda x: x[0])
            mptcpsec_client_throughput_list_temp = sorted(mptcpsec_client_throughput_list_temp, key=lambda x: x[0])
            mptcp_client_throughput_list_temp = sorted(mptcp_client_throughput_list_temp, key=lambda x: x[0])
            tls_server_throughput_list_temp = sorted(tls_server_throughput_list_temp, key=lambda x: x[0])
            mptcpsec_server_throughput_list_temp = sorted(mptcpsec_server_throughput_list_temp, key=lambda x: x[0])
            mptcp_server_throughput_list_temp = sorted(mptcp_server_throughput_list_temp, key=lambda x: x[0])
            print("After sorting - tls_client_throughput_list_temp = " + str(tls_client_throughput_list_temp))

            data_transferred = [pair[0] / 10**6 for pair in tls_client_throughput_list_temp]
            tls_client_throughput_list = [pair[1] * 8 / 10**6 for pair in tls_client_throughput_list_temp]
            mptcpsec_client_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcpsec_client_throughput_list_temp]
            mptcp_client_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcp_client_throughput_list_temp]
            tls_server_throughput_list = [pair[1] * 8 / 10**6 for pair in tls_server_throughput_list_temp]
            mptcpsec_server_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcpsec_server_throughput_list_temp]
            mptcp_server_throughput_list = [pair[1] * 8 / 10**6 for pair in mptcp_server_throughput_list_temp]
            print("After division to put it in MB - tls_client_throughput_list = " + str(tls_client_throughput_list))

            # Build throughput graph (w.r.t. the data transfer size)
            graph_client_comparison_title = "Throughput from client perspective\n"
            graph_client_comparison_file_name = "client_throughput_comparison_" + losses_str + ".svg"
            graph_server_comparison_title = "Throughput from server perspective\n"
            graph_server_comparison_file_name = "server_throughput_comparison_" + losses_str + ".svg"

            self.plot_throughput_comparison(data_transferred, tls_client_throughput_list,
                                            mptcpsec_client_throughput_list, mptcp_client_throughput_list,
                                            title=graph_client_comparison_title,
                                            graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                        graph_client_comparison_file_name))

            self.plot_throughput_comparison(data_transferred, tls_server_throughput_list,
                                            mptcpsec_server_throughput_list, mptcp_server_throughput_list,
                                            title=graph_server_comparison_title,
                                            graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                        graph_server_comparison_file_name))

    @staticmethod
    def plot_throughput_comparison(data_transferred, tls_times, mptcpsec_times, mptcp_times, title, graph_dst_file):

        # new frame
        fig = plt.figure()

        # plot data
        if len(data_transferred) < 200:
            plt.plot(data_transferred, tls_times, 'ok', label="TLS", color=colors_dict[TLS])
            plt.plot(data_transferred, mptcpsec_times, 'ok', label="MPTCPsec", color=colors_dict[MPTCPSEC])
            plt.plot(data_transferred, mptcp_times, 'ok', label="MPTCP", color=colors_dict[MPTCP])
        else:
            plt.plot(data_transferred, tls_times, label="TLS", color=colors_dict[TLS])
            plt.plot(data_transferred, mptcpsec_times, label="MPTCPsec", color=colors_dict[MPTCPSEC])
            plt.plot(data_transferred, mptcp_times, label="MPTCP", color=colors_dict[MPTCP])

        # add in labels and title
        plt.xlabel("Data transferred (MB)")
        plt.ylabel("Throughput (Mb/s)")
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # Dezoom the graph
        # plt.ylim(75, 100)

        # Correct space
        plt.tight_layout()

        # save figure
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)

    @staticmethod
    def plot_data_transferred_comparison(data_size, tls_times, mptcpsec_times, mptcp_times, title,
                                         graph_dst_file):

        data_transferred = [y*4096 for y in range(len(mptcpsec_times))]
        if data_size % 4096 != 0:
            data_transferred[-1] = data_size

        data_transferred = [y/1000.0 for y in data_transferred]

        # new frame
        fig = plt.figure()

        # plot data
        if len(data_transferred) < 200:
            plt.plot(tls_times, data_transferred, 'k', label="TLS", color=colors_dict[TLS])
            plt.plot(mptcpsec_times, data_transferred, 'k', label="MPTCPsec", color=colors_dict[MPTCPSEC])
            plt.plot(mptcp_times, data_transferred, 'k', label="MPTCP", color=colors_dict[MPTCP])
        else:
            plt.plot(tls_times, data_transferred, label="TLS", color=colors_dict[TLS])
            plt.plot(mptcpsec_times, data_transferred, label="MPTCPsec", color=colors_dict[MPTCPSEC])
            plt.plot(mptcp_times, data_transferred, label="MPTCP", color=colors_dict[MPTCP])

        # add in labels and title
        plt.xlabel("Time (ms) starting from the end of the connection establishment")
        plt.ylabel("Transferred Data (MB)")
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # Correct space
        plt.tight_layout()

        # save figure
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)


class ProcessingMeasureHandler(CaptureHandler):

    def __init__(self):
        super(ProcessingMeasureHandler, self).__init__(protocol_used=[TLS, MPTCPSEC, MPTCP],
                                                       input_sub_folder="pingpong",
                                                       graph_sub_folder="processing_measure",
                                                       method="Processing measure")

    def dispatch_file_handler(self, graph_directory, file_name):
        return None if file_name.find(".pcap") < 0 else PingPongCaptureFileHandler(graph_directory, file_name)

    @staticmethod
    def interpret_cap_files(file_handlers):

        ping_pong_time_list = []
        for file_handler in file_handlers:
            if file_handler.ping_pong_time > 0:
                ping_pong_time_list.append(file_handler.ping_pong_time)

        if len(ping_pong_time_list) == 0:
            return -1

        ping_pong_time_list = sorted(ping_pong_time_list)

        if len(ping_pong_time_list) % 2 != 0:
            median = ping_pong_time_list[len(ping_pong_time_list) // 2]
        else:
            median = float(ping_pong_time_list[len(ping_pong_time_list) // 2] +
                           ping_pong_time_list[(len(ping_pong_time_list) // 2) + 1]) / 2.0
        return median

    def handle(self, script_path):

        super(ProcessingMeasureHandler, self).handle(script_path)

        graph_directory = os.path.join(os.path.dirname(script_path), self.graph_sub_folder)

        tls_server_dict = self.capture_dictionaries[TLS][SERVER]
        mptcpsec_server_dict = self.capture_dictionaries[MPTCPSEC][SERVER]
        mptcp_server_dict = self.capture_dictionaries[MPTCP][SERVER]

        # Sort file handlers
        for file_handler in self.file_handlers:

            bytes_transferred = file_handler.byte_transferred
            if bytes_transferred <= 0 or file_handler.protocol not in self.capture_dictionaries or \
                    not file_handler.is_server_file or file_handler.losses_str != NO_LOSSES_STR:
                continue

            dictionary = self.capture_dictionaries[file_handler.protocol][SERVER]
            if file_handler.byte_transferred not in dictionary[file_handler.losses_str]:
                dictionary[file_handler.byte_transferred] = [file_handler]
            else:
                dictionary[file_handler.byte_transferred].append(file_handler)

        # Interpret data
        tls_server_pairs = []
        mptcpsec_server_pairs = []
        mptcp_server_pairs = []

        for key, value in mptcpsec_server_dict.items():

            if key in tls_server_dict and key in mptcp_server_dict:
                tls_server_pairs.append((key, self.interpret_cap_files(tls_server_dict[key])))
                mptcpsec_server_pairs.append((key, self.interpret_cap_files(mptcpsec_server_dict[key])))
                mptcp_server_pairs.append((key, self.interpret_cap_files(mptcp_server_dict[key])))

        # Order measures
        tls_server_pairs = sorted(tls_server_pairs, key=lambda x: x[0])
        print("TLS = " + str(tls_server_pairs))
        mptcpsec_server_pairs = sorted(mptcpsec_server_pairs, key=lambda x: x[0])
        print("MPTCPsec = " + str(mptcpsec_server_pairs))
        mptcp_server_pairs = sorted(mptcp_server_pairs, key=lambda x: x[0])
        print("MPTCP = " + str(mptcp_server_pairs))

        sent_sizes = [pair[0] for pair in mptcpsec_server_pairs]
        tls_server_durations = [pair[1] for pair in tls_server_pairs]
        mptcpsec_server_durations = [pair[1] for pair in mptcpsec_server_pairs]
        mptcp_server_durations = [pair[1] for pair in mptcp_server_pairs]

        # Build comparison graph between TLS and MPTCPsec for each different transfer size
        graph_server_comparison_title = "Echo and reply of different sizes\n"
        graph_server_comparison_file_name = "pingpong_server_comparison.svg"

        self.plot_processing_comparison(sent_sizes, tls_server_durations, mptcpsec_server_durations,
                                        mptcp_server_durations, title=graph_server_comparison_title,
                                        graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                    graph_server_comparison_file_name))

    @staticmethod
    def plot_processing_comparison(sent_sizes, tls_durations, mptcpsec_durations, mptcp_durations, title,
                                   graph_dst_file):

        # new frame
        fig = plt.figure()

        # plot data
        if len(sent_sizes) < 200:
            plt.plot(sent_sizes, tls_durations, 'ko', label="TLS", color=colors_dict[TLS])
            plt.plot(sent_sizes, mptcpsec_durations, 'ko', label="MPTCPsec", color=colors_dict[MPTCPSEC])
            plt.plot(sent_sizes, mptcp_durations, 'ko', label="MPTCP", color=colors_dict[MPTCP])
        else:
            plt.plot(sent_sizes, tls_durations, label="TLS", color=colors_dict[TLS])
            plt.plot(sent_sizes, mptcpsec_durations, label="MPTCPsec", color=colors_dict[MPTCPSEC])
            plt.plot(sent_sizes, mptcp_durations, label="MPTCP", color=colors_dict[MPTCP])

        # add in labels and title
        plt.xlabel("Message data size (bytes)")
        plt.ylabel("Time elapsed (ms)")
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # Correct space
        plt.tight_layout()

        # save figure
        graph_dst_base_name = os.path.basename(graph_dst_file)
        graph_dst_base_name = (str(graph_dst_base_name.split(".")[0]) + "_window." +
                               str(graph_dst_base_name.split(".")[1]))
        graph_dst_file = os.path.join(os.path.dirname(graph_dst_file), graph_dst_base_name)
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)


class MeasureUnderAttackHandler(TimeMeasureHandler):

    def __init__(self):

        super(MeasureUnderAttackHandler, self).__init__(protocol_used=[TLS, MPTCPSEC],
                                                        input_sub_folder="bulk_attack", graph_sub_folder="bulk_attack",
                                                        method="Attacks")

    def dispatch_file_handler(self, graph_directory, file_name):
        return TimeFileHandler(graph_directory, file_name)

    def handle(self, script_path):

        # Create file handlers
        super(MeasureUnderAttackHandler, self).handle(script_path)

        graph_directory = os.path.join(os.path.dirname(script_path), self.graph_sub_folder)

        # Sort file handlers
        for file_handler in self.file_handlers:

            bytes_transferred = file_handler.byte_transferred
            if bytes_transferred <= 0 or file_handler.protocol not in self.time_dictionaries:
                continue

            dictionary = self.time_dictionaries[file_handler.protocol][
                SERVER if file_handler.is_server_file else CLIENT]
            if file_handler.losses_str not in dictionary:
                dictionary[file_handler.losses_str] = {file_handler.byte_transferred: [file_handler]}
            elif file_handler.byte_transferred not in dictionary[file_handler.losses_str]:
                dictionary[file_handler.losses_str][file_handler.byte_transferred] = [file_handler]
            else:
                dictionary[file_handler.losses_str][file_handler.byte_transferred].append(file_handler)

        # Interpret data
        for losses_str, value in self.time_dictionaries[self.protocol_used[0]][SERVER].items():

            for byte_transferred, file_handler_list in value.items():

                # Check that the file_handler is available
                available = True
                for protocol in self.protocol_used:
                    if byte_transferred not in self.time_dictionaries[protocol][SERVER][losses_str] or \
                       byte_transferred not in self.time_dictionaries[protocol][CLIENT][losses_str]:
                        available = False
                        break
                if not available:
                    continue

                # Get data for graph of the data transferred over the time
                tls_client_times = self.time_dictionaries[TLS][CLIENT][losses_str][byte_transferred][0].interpret_file()
                tls_server_times = self.time_dictionaries[TLS][SERVER][losses_str][byte_transferred][0].interpret_file()
                mptcpsec_client_times = self.time_dictionaries[MPTCPSEC][CLIENT][losses_str][byte_transferred][0]\
                    .interpret_file()
                mptcpsec_server_times = self.time_dictionaries[MPTCPSEC][SERVER][losses_str][byte_transferred][0]\
                    .interpret_file()

                # Build graphs of data transferred over the time
                graph_client_comparison_title = "Transferred data from client perspective\n"
                graph_client_comparison_file_name = "client_attack_measure_" + losses_str + "_" + \
                                                    str(byte_transferred) + ".svg"
                graph_server_comparison_title = "Transferred Data from server perspective\n"
                graph_server_comparison_file_name = "server_attack_measure_" + losses_str + "_" + \
                                                    str(byte_transferred) + ".svg"

                self.plot_data_transferred_comparison(byte_transferred, tls_client_times, mptcpsec_client_times,
                                                      title=graph_client_comparison_title,
                                                      graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                  graph_client_comparison_file_name))
                self.plot_data_transferred_comparison(byte_transferred, tls_server_times, mptcpsec_server_times,
                                                      title=graph_server_comparison_title,
                                                      graph_dst_file=os.path.join(graph_directory, RESULTS_SUB_FOLDER,
                                                                                  graph_server_comparison_file_name))

    @staticmethod
    def plot_data_transferred_comparison(data_size, tls_times, mptcpsec_times, title, graph_dst_file):

        data_transferred_tls = [y*4096 for y in range(len(tls_times))]
        if data_transferred_tls[-1] > data_size:
            data_transferred_tls[-1] = data_size
        data_transferred_tls = [y/1000.0 for y in data_transferred_tls]
        data_transferred_mptcpsec = [y*4096 for y in range(len(mptcpsec_times))]
        if data_transferred_mptcpsec[-1] > data_size:
            data_transferred_mptcpsec[-1] = data_size
        data_transferred_mptcpsec = [y/1000.0 for y in data_transferred_mptcpsec]

        # new frame
        fig = plt.figure()

        # plot data
        if len(data_transferred_tls) < 200 or len(data_transferred_mptcpsec) < 200:
            plt.plot(tls_times, data_transferred_tls, 'ok', label="TLS", color=colors_dict[TLS])
            plt.plot(mptcpsec_times, data_transferred_mptcpsec, 'ok', label="MPTCPsec", color=colors_dict[MPTCPSEC])
        else:
            plt.plot(tls_times, data_transferred_tls, color=colors_dict[TLS])
            plt.plot(mptcpsec_times, data_transferred_mptcpsec, color=colors_dict[MPTCPSEC])

        # add in labels and title
        plt.xlabel("Time (ms) starting from the end of the connection establishment")
        plt.ylabel("Transferred Data (kB)")
        plt.title(title)

        # create legend
        plt.legend(loc="upper left")

        # Correct space
        plt.tight_layout()

        # save figure
        os.makedirs(os.path.dirname(graph_dst_file), mode=0o777, exist_ok=True)
        plt.savefig(graph_dst_file)

        # Remove figure
        plt.close(fig)


rcParams.update({'font.size': 17})
handlers = [MeasureUnderAttackHandler(), ThroughputMeasureHandler()]
