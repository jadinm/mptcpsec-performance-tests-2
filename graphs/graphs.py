#!/usr/bin/env python3

import sys
import os
import time
import shutil

from sshConnection import is_ip_address, connect, logout, download_file, download_directory
from data_handlers import handlers

SRC_DIRECTORY = os.path.join(os.path.dirname(os.path.dirname(sys.argv[0])))
CLIENT_DIRECTORY = "client"
SERVER_DIRECTORY = "server"
GRAPH_DIRECTORY = "graphs"


def copy_tree(ssh_client, src_directory, dst_directory):

    if ssh_client is None:  # Local directory
        for (dir_path, dir_names, file_names) in os.walk(src_directory):
            for file_name in file_names:
                local_path = os.path.join(dst_directory, file_name)
                os.makedirs(os.path.dirname(local_path), mode=0o777, exist_ok=True)
                shutil.copy(src=os.path.join(src_directory, file_name), dst=local_path)
    else:  # Remote directory
        download_directory(client=client, remote_path=src_directory, local_path=dst_directory)


def show_help():
    print("Usage :")
    print("Usage : ./graph.py [localBinDirectory remoteBinDirectory remoteUser remoteIP rsaKeyFile hostKeyPath]"
          " isClientLocal ]\n")
    print("isClientLocal = True|False whether the client was the localhost or not")
    print("If no option is provided, no file transfer is made (only graph production)")
    print("To print this message : ./graph.py -h\n")

# Arguments parsing
if len(sys.argv) != 1:
    if sys.argv[1] == "-h" or len(sys.argv) < 8:
        show_help()
        sys.exit(1)

    localBinDirectory = sys.argv[1]
    if not os.path.isdir(localBinDirectory):
        print("ERROR : Local bin directory " + sys.argv[1] + " doesn't exist !")
        show_help()
        sys.exit(1)

    remoteBinDirectory = sys.argv[2]
    remoteUser = sys.argv[3]

    if not is_ip_address(sys.argv[4]):
        print("ERROR : Bad IP address !")
        show_help()
        sys.exit(1)
    ip_address = sys.argv[4]

    rsaKeyFile = sys.argv[5]

    if not os.path.isfile(sys.argv[6]):
        print("ERROR : host keys file " + sys.argv[6] + " doesn't exist !")
        show_help()
        sys.exit(1)
    host_keys_path = sys.argv[6]

    isClientLocal = sys.argv[7] == "True"

    # Start SSH connection
    client = connect(ip_address, remoteUser, rsaKeyFile, host_keys_path)
    if client is None:
        sys.exit(1)
else:
    client = None


start_execution = time.time()

for handler in handlers:

    print("We are creating graphs for performance test : " + handler.method)

    if client is not None:

        clientBinDirectory = localBinDirectory if isClientLocal else remoteBinDirectory
        serverBinDirectory = localBinDirectory if not isClientLocal else remoteBinDirectory

        src_client_data_directory = os.path.join(clientBinDirectory, CLIENT_DIRECTORY, handler.input_sub_folder)
        dst_client_data_directory = os.path.join(SRC_DIRECTORY, GRAPH_DIRECTORY, handler.graph_sub_folder)
        copy_tree(None if isClientLocal else client, src_client_data_directory, dst_client_data_directory)

        src_server_data_directory = os.path.join(serverBinDirectory, SERVER_DIRECTORY, handler.input_sub_folder)
        dst_server_data_directory = os.path.join(SRC_DIRECTORY, GRAPH_DIRECTORY, handler.graph_sub_folder)
        copy_tree(None if not isClientLocal else client, src_server_data_directory, dst_server_data_directory)

    handler.handle(sys.argv[0])


# Close the SSH connection
if client is not None:
    logout(client)

execution_time = int(time.time() - start_execution)

print("\nThe whole script took " + str(execution_time) + " seconds.")
