#ifndef PERF_PINGPONG_H
#define PERF_PINGPONG_H

int client_pingpong(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                    long iterations, long no_capture);
int server_pingpong(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                    long iterations, long no_capture);

#endif //PERF_PINGPONG_H
