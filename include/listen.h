#ifndef PERF_LISTEN_H
#define PERF_LISTEN_H

#include <openssl/bio.h>

int mptcpsec_listen(const char *serverIP, const char *serverPort, int *listening_sfd);
BIO *tls_listen(const char *serverIP, const char *serverPort, BIO **listening_bio);
int mptcp_listen(const char *serverIP, const char *serverPort, int *listening_sfd);
int mptcpsec_accept(int sfd);
BIO *tls_accept(BIO *bio);
int mptcp_accept(int sfd);

#endif //PERF_LISTEN_H
