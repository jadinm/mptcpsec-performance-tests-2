#ifndef PERF_CONNECT_H
#define PERF_CONNECT_H

#include <openssl/bio.h>

int mptcpsec_connect(const char *serverIP, const char *serverPort);
BIO *tls_connect(const char *serverIP, const char *serverPort);
int mptcp_connect(const char *serverIP, const char *serverPort);

#endif //PERF_CONNECT_H
