#ifndef PERF_MAIN_H
#define PERF_MAIN_H

#include <stdio.h>
#include <string.h>

#include "../include/list.h"

#define MPTCPSEC    0
#define TLS         1
#define MPTCP       2

#define MPTCP_SECURITY_PREFERENCE 43
#define MPTCP_ENCR_MUST 2

#define MAX_PATH_SIZE 1000
#define SERVER_CERTIFICATE "server.pem"
#define CAPTURE_PROGRAM "tcpdump_capture_packets.py"

#define START_HELP_LINE "                 "


char executable_directory [MAX_PATH_SIZE];
char certificate_path [MAX_PATH_SIZE];
char capture_path [MAX_PATH_SIZE];
char output_directory [MAX_PATH_SIZE];

struct list *list_losses_server;
struct list *list_losses_client;
int losses_to_remove;

static inline char *certificate_full_path(void) {

    snprintf(certificate_path, MAX_PATH_SIZE, "%s/%s", executable_directory, SERVER_CERTIFICATE);
    return certificate_path;
}

static inline char *capture_full_path(void) {

    snprintf(capture_path, MAX_PATH_SIZE, "%s/%s", executable_directory, CAPTURE_PROGRAM);
    return capture_path;
}

/* I/O prototypes */

int argument_parsing(int argc, char *argv [], int *no_capture, long *bytes_under_attack, long *start_bulk,
                     long *step_bulk, long *end_bulk, long *start_pingpong, long *step_pingpong, long *end_pingpong,
                     long *iterations, char **serverIP, char **serverPort, int *protocol);

void pingpong_output_capture_file(int client, int protocol, int iteration_number, long no_capture,
                                  long bytes_to_exchange, char *capture_path);

int bulk_store_measures(struct timeval *results, size_t results_length, int server, int protocol,
                                      long bytesToExchange, int repeat_number);
int bulk_attack_store_measures(struct timeval *results, size_t results_length, int server, int protocol,
                               long bytes_to_exchange);
int pingpong_store_measures(struct timeval *results, size_t results_length, int client, int protocol,
                            int iteration_number, long no_capture, long start, long step);

#endif //PERF_MAIN_H
