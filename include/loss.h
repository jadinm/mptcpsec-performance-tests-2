#ifndef PERF_LOSS_H
#define PERF_LOSS_H

int apply_loss_settings(struct list *loss_list);
int remove_loss_settings(struct list *loss_list);
void set_loss_subdirectory_name(struct list *client_loss_list, struct list *server_loss_list, char *subdirectory);

#endif //PERF_LOSS_H
