#ifndef PERF_LIST_H
#define PERF_LIST_H

#include <stdlib.h>

#define MAX_INTERFACE_SIZE 20
#define list_walk_safe(queue, current, temp) \
        for (current = (&(queue->list))->prev, temp = current->next; \
             current != (&(queue->list)); \
             current = temp, temp = current->next)

struct list_header {
    struct list_header *prev;
    struct list_header *next;
};

struct list {
    struct list_header list;
    size_t length;
};

struct loss_element {
    struct list_header list;
    char intf[MAX_INTERFACE_SIZE+1];
    float loss;
};

struct list *init_list(void);
void free_list(struct list *list);
void queue_tail(struct list *list, struct list_header *last);

struct loss_element *init_loss_element(struct list *list, char *intf, float loss);

#endif //PERF_LIST_H
