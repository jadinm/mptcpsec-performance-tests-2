#ifndef PERF_DATA_TRANSFER_H
#define PERF_DATA_TRANSFER_H

#include <openssl/bio.h>

int free_structures(int sfd, BIO *bio, int listen_only);
int send_data(long buff_size, int sfd, BIO *bio, char *buffer, int protocol);
int recv_data(long buff_size, int sfd, BIO *bio, char *buffer, int protocol);
int new_connection(int client, int protocol, const char *serverIP, const char *serverPort, int *sfd, BIO **bio,
                   int *listen_sfd, BIO **listening_bio, char *buffer);

#endif //PERF_DATA_TRANSFER_H
