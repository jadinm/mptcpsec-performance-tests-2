#ifndef PERF_THROUGHPUT_MEASURE_H
#define PERF_THROUGHPUT_MEASURE_H

int client_bulk_loop(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                     long repeat);
int server_bulk_loop(int protocol, const char *serverIP, const char *serverPort, long start, long step, long end,
                     long repeat);
int bulk_attack(int client, int protocol, const char *serverIP, const char *serverPort, long bytes_under_attack);

#endif //PERF_THROUGHPUT_MEASURE_H
