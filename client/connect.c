#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"

#include "../include/main.h"

/**
 * Code Inspired from the man page of getaddrinfo()
 * (see http://linux.die.net/man/3/getaddrinfo)
 */
int mptcpsec_connect(const char *serverIP, const char *serverPort) {

    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd = -1;
    int s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC; /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* TCP socket */
    hints.ai_flags = 0;
    hints.ai_protocol = 0; /* Any protocol */

    s = getaddrinfo(serverIP, serverPort, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -2;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                     rp->ai_protocol);
        if (sfd == -1)
            continue;

        int val = 1;
        socklen_t sock_opt_va_len = sizeof(val);
        if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &val, sock_opt_va_len)) {
            fprintf(stderr, "Could not reuse !\n");
            close(sfd);
            continue;
        }

        int sock_opt_val = MPTCP_ENCR_MUST;
        if (setsockopt(sfd, IPPROTO_TCP, MPTCP_SECURITY_PREFERENCE, &sock_opt_val, sizeof(sock_opt_val))) {
            fprintf(stderr, "setsockopt failed !\n");
            continue;
        }
        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
            break; /* Success */

        close(sfd);
    }

    if (rp == NULL) { /* No address succeeded */
        fprintf(stderr, "Could not connect\n");
        return -1;
    }

    freeaddrinfo(result); /* No longer needed */

    return sfd;
}

/**
 * Code Inspired from the man page of getaddrinfo()
 * (see http://linux.die.net/man/3/getaddrinfo)
 */
int mptcp_connect(const char *serverIP, const char *serverPort) {

    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd = -1;
    int s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC; /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* TCP socket */
    hints.ai_flags = 0;
    hints.ai_protocol = 0; /* Any protocol */

    s = getaddrinfo(serverIP, serverPort, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -2;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                     rp->ai_protocol);
        if (sfd == -1)
            continue;

        int val = 1;
        socklen_t sock_opt_va_len = sizeof(val);
        if (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &val, sock_opt_va_len)) {
            fprintf(stderr, "Could not reuse !\n");
            close(sfd);
            continue;
        }

        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
            break; /* Success */

        close(sfd);
    }

    if (rp == NULL) { /* No address succeeded */
        fprintf(stderr, "Could not connect\n");
        return -1;
    }

    freeaddrinfo(result); /* No longer needed */

    return sfd;
}

/**
 * Code inspired from the man page of bio_new_ssl(3)
 * (see http://linux.die.net/man/3/bio_new_ssl)
 */
BIO *tls_connect(const char *serverIP, const char *serverPort) {

    fflush(stdout);
    sleep(3);

    BIO * bio;
    SSL_CTX * ctx = SSL_CTX_new(TLSv1_2_client_method());

    size_t address_len = strlen(serverIP) + strlen(serverPort) + 10;
    char *address = (char *) malloc(address_len);
    if (!address) {
        fprintf(stderr, "Error allocation address space\n");
        return NULL;
    }

    snprintf(address, address_len, "%s:%s", serverIP, serverPort);

    fflush(stdout);

    SSL * ssl;
    bio = BIO_new_ssl_connect(ctx);

    fflush(stdout);

    BIO_get_ssl(bio, &ssl);
    if (!ssl) {
        fprintf(stderr, "Can't locate SSL pointer\n");
        free(address);
        return NULL;
    }

    /* Don't want any retries */
    SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

    /* Force the cipher to AES128-GCM in order to be able to compare it
     * to MPTCPsec that only support this algorithm for now
     */
    SSL_set_cipher_list(ssl, "AES128-GCM-SHA256:DH-RSA-AES128-GCM-SHA256:DH-DSS-AES128-GCM-SHA256:"
            "DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:"
            "ECDHE-ECDSA-AES128-GCM-SHA256:ADH-AES128-GCM-SHA256");

    BIO_set_conn_hostname(bio, address);

    if (BIO_do_connect(bio) <= 0) {
        fprintf(stderr, "Error connecting to server\n");
        free(address);
        ERR_print_errors_fp(stderr);
        return NULL;
    }

    if (BIO_do_handshake(bio) <= 0) {
        fprintf(stderr, "Error establishing SSL connection\n");
        free(address);
        ERR_print_errors_fp(stderr);
        return NULL;
    }

    free(address);
    return bio;
}
